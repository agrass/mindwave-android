package com.enhanceye.mindwavelogger;

import android.widget.Button;
import android.widget.TextView;

public class InterfaceMgr {
	
	public TextView tv;
	public TextView scrn;
	public TextView math;
	public Button button;	
	public InterfaceMgr(TextView tv1, TextView scr, TextView mth, Button btn){
		this.tv = tv1;
		this.scrn = scr;
		this.math = mth;
		this.button = btn;
		
	}

}
