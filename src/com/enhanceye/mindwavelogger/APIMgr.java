package com.enhanceye.mindwavelogger;

import java.util.Calendar;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

public class APIMgr {	
	public static String host = "http://mindwave.herokuapp.com/sendData";
	public Calendar t = Calendar.getInstance();
	public APIMgr(){
		t = Calendar.getInstance();
	}
	public void send_value(String tipo, float value){	
		t = Calendar.getInstance();		
		AsyncHttpClient client = new AsyncHttpClient();
		client.get(host +"?valor="+ value+ "&name="+ tipo + "&hora="+ t.getTime() , new AsyncHttpResponseHandler() {
			@Override
	    	public void onSuccess(String response) {
	        //System.out.println(response);
	    }
		});  
	}	
	public void send_value(float delta, float highalpha, float highbeta, float lowalpha, float lowbeta, float lowgama, float midgama, float theta ){	
		t = Calendar.getInstance();		
		AsyncHttpClient client = new AsyncHttpClient();
		client.get(host +"?delta="+ delta+ "&halpha="+ highalpha + "&hbeta=" + highbeta + "&lalpha=" + lowalpha + "&lbeta=" + lowbeta + "&lgama=" + lowgama + "&mgama=" + midgama+ "theta="+ theta +  "&hora="+ t.getTime() , new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(String response) {
	        //System.out.println(response);
			}
		});         
	}
}
