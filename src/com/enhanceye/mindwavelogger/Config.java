package com.enhanceye.mindwavelogger;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.NumberPicker;
import android.widget.NumberPicker.OnValueChangeListener;
import android.widget.Switch;
import android.widget.Toast;
import android.widget.ToggleButton;

public class Config extends Activity implements NumberPicker.OnValueChangeListener {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_config);  
        NumberPicker np = (NumberPicker) findViewById(R.id.numberPicker1);
        np.setMaxValue(99);
        np.setMinValue(0);
        np.setValue(3);
        np.setOnValueChangedListener(this);  
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_config, menu);
        return true;
    }
    
    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
        MindWaveMgr.delay = newVal;
    }
    
    public void onToggleClicked(View view) {
        // Is the toggle on?
        boolean on = ((ToggleButton) view).isChecked();
        
        if (on) {
            MindWaveMgr.select_mode = 1;
        } else {
        	 MindWaveMgr.select_mode = 0;
        }
    }
    public void startApp(View view) {
    	Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
    }
}
