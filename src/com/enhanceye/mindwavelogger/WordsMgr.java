package com.enhanceye.mindwavelogger;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.SearchManager;
import android.content.SearchRecentSuggestionsProvider;
import android.content.res.AssetManager;

public class WordsMgr {
	public List<String> abc;	
	public String current_word = "";
	public String current_letter = "";
	public String previous_letter = "";
	boolean backspace = true;
	int confirmation_times = 2;
	boolean space = true;
	public SearchManager sm;
	public int index = 0;	
	public HashMap<String, Integer> cache = new HashMap<String, Integer>();
	public Trie root;
	int collection_index = 0;
	AssetManager am;
	List<String> letters;
	
	public WordsMgr(AssetManager am2){		
		//sm = new SearchRecentSuggestionsProvider();
		this.am = am2;
		root = new Trie();
		addWords();		
		buildABC();
    	letters = root.autoComplete("");
    	nextLetter();
	}
	
	public void pushWord(){
		if(current_letter.equals("Backspace")){
			current_word.substring(0, current_word.length() - 1);
		}
		else if(current_letter.equals("Space")){
			current_word += " ";
		}
		else if(cache.get(current_letter) == null || cache.get(current_letter) ==0 ){		
			cache.put(current_letter, 1);		
		}
		else{
			cache.put(current_letter, cache.get(current_letter) + 1);
		}
		if(cache.get(current_letter) >= confirmation_times){
			current_word += current_letter;
			cache.put(current_letter, 0);
			letters = root.autoComplete(current_word);
			collection_index = 0;
			index = 0;
		}
	}
	
	public String currentWord(){
		return current_letter;
	}
	
	public String currentLetter(){
		return current_letter;
	}
	public void nextLetter(){
		if(index == 25){
			collection_index = 0;
			index = 0;
			backspace= true;
			space = true;
		}
		previous_letter = current_letter;		
		if(collection_index < letters.size()){
			current_letter =  letters.get(collection_index);
			collection_index++;
		}
		else{
			if(backspace){
				current_letter = "Backspace";
				backspace = false;
			}
			else if(space){
				current_letter = "Space";
				space = false;
			}
			else{
				while(letters.contains(abc.get(index))) index = (index + 1)%26;
				current_letter = abc.get(index);
				index = (index + 1)%26;	
			}
		}		
	}
	
	public int getLetterValue(){
		return (Character.getNumericValue(current_letter.toCharArray()[0]) -10);
	}
	
	public void clearALL(){
		current_word = "";
		current_letter = "";
		previous_letter = "";
		collection_index = 0;
		index = 0;
		letters = root.autoComplete("");
		nextLetter();		
	}
	
	//Add a dictionary of most commun words. the dictionary its in assets/words.txt and the letters are displayed in the order given
	public void addWords() {		
		InputStream is;
		try {
			is = am.open("words.txt");		
		BufferedReader br = new BufferedReader(new InputStreamReader(is));		
		  String strLine;
		  //Read File Line By Line
		  while ((strLine = br.readLine()) != null)   {
		  // Print the content on the console
		  System.out.println (strLine);
		  root.insert(strLine);
		  }
		  is.close();
		  
		} catch (IOException e) {
		}			
	}
	
	public void buildABC(){
		abc = Arrays.asList("a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z" );
	}

}
