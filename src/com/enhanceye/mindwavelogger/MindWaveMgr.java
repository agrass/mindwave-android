package com.enhanceye.mindwavelogger;

import java.util.Calendar;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import android.bluetooth.BluetoothAdapter;
import android.graphics.Color;
import android.os.Handler;
import android.os.Message;
import android.speech.tts.TextToSpeech;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.heroicrobot.dropbit.devices.pixelpusher.Strip;
import com.neurosky.thinkgear.TGDevice;
import com.neurosky.thinkgear.TGRawMulti;

public class MindWaveMgr {
	TGDevice tgDevice;
	BluetoothAdapter btAdapter;
	TGRawMulti tgRaw;	
	double att = 0;
	double med = 0;
	static int select_mode = 0;
	static int delay = 3;
	static double focus_avg = 0;
	//standar desviation
	static double focus_std = 15;
	int n_avg = 0;
	static double relax_avg = 0;
	static double relax_std = 15;
	//14 vaues to check  (7 of attention and 7 of meditation)
	int values_left = 40;
	double blk = 0;
	boolean active = false;
	MainActivity ma;
	Random rand = new Random();
	int count_blink = 0;
	int blink_avg = 0;    
    int buttonState = 0;    
	private InterfaceMgr im;;
	private APIMgr apim;	
	private WordsMgr wm;
	public MindWaveMgr(InterfaceMgr imt, APIMgr apmg, WordsMgr wmr, MainActivity mat){
		tgRaw = new TGRawMulti();   
		this.im = imt;
		this.apim = apmg;
		this.wm = wmr;
		this.ma = mat;
		btAdapter = BluetoothAdapter.getDefaultAdapter();    	
    	if(btAdapter != null) {
    	tgDevice = new TGDevice(btAdapter, handler);
//        tgDevice.connect(true);
//        tgDevice.start();
    	} 
	}
	
    private final Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
        	switch (msg.what) {
            case TGDevice.MSG_STATE_CHANGE:

                switch (msg.arg1) {
	                case TGDevice.STATE_IDLE:
	                    break;
	                case TGDevice.STATE_CONNECTING:		                	
	                	im.tv.append("Connecting...\n");
	                	break;		                    
	                case TGDevice.STATE_CONNECTED:
	                	im.tv.append("Connected.\n");
	                	im.scrn.setText("Connecting..");
	                	//setTimmer();
	                	tgDevice.start();
	                    break;
	                case TGDevice.STATE_NOT_FOUND:
	                	im.tv.append("Can't find\n");
	                	break;
	                case TGDevice.STATE_NOT_PAIRED:
	                	im.tv.append("not paired\n");
	                	break;
	                case TGDevice.STATE_DISCONNECTED:
	                	im.tv.append("Disconnected\n");
                }

                break;
            case TGDevice.MSG_POOR_SIGNAL:
            		//signal = msg.arg1;
            		im.tv.append("PoorSignal: " + msg.arg1 + "\n");
                break;
            case TGDevice.MSG_RAW_DATA:	  
            		//raw1 = msg.arg1;
            		//tv.append("Got raw: " + msg.arg1 + "\n");
    
            	break;
            case TGDevice.MSG_HEART_RATE:
        		//tv.append("Heart rate: " + msg.arg1 + "\n");
                break;
            case TGDevice.MSG_ATTENTION:
            		att = msg.arg1 * 2.55;
            		im.tv.append("Attention: " + msg.arg1 + "\n");            		
            		//tv.append("Attention Color: " + att + "\n");
            		//checkAttention();
            		if(active) apim.send_value("att", msg.arg1);
            		//writeExtFile("Attention: " + msg.arg1 + " @" + t.get(Calendar.HOUR) + ":" + t.get(Calendar.MINUTE) + ":" + t.get(Calendar.SECOND) + t.get(Calendar.AM_PM) + " " + "\n");
            		//Log.v("HelloA", "Attention: " + att + "\n");
            		im.tv.setBackgroundColor(Color.argb(255, (int)att, 0, 0));
            	break;
            
            case TGDevice.MSG_MEDITATION:
            	med = msg.arg1 * 2.55;
            	im.tv.append("Meditation: " + msg.arg1 + "\n");
            	if(active){
            		apim.send_value("med", msg.arg1);
            		checkAttention();            	
            	}
            	else preStart();
        		//t = Calendar.getInstance();        		
        		//writeExtFile("Meditation: " + msg.arg1 + " @" + t.get(Calendar.HOUR) + ":" + t.get(Calendar.MINUTE) + ":" + t.get(Calendar.SECOND) + t.get(Calendar.AM_PM) + " " + "\n");
            	break;
            case TGDevice.MSG_EEG_POWER:            	
            	//Log.v("HelloEEG", "Delta: " + msg.arg1);
            	//TGEegPower ep = (TGEegPower)msg.obj;
            	//send_value(host, ep.delta, ep.highAlpha, ep.highBeta, ep.lowAlpha, ep.lowBeta, ep.lowGamma, ep.midGamma, ep.theta);			
            case TGDevice.MSG_BLINK:
            		im.tv.append("Blink: " + msg.arg1 + "\n");
            		blk = msg.arg1;             		
            		if(active){
            			checkBlink();
            			apim.send_value("bk", msg.arg1);            	            		
            		}
            	break;
            case TGDevice.MSG_RAW_COUNT:
            	break;
            case TGDevice.MSG_LOW_BATTERY:
            	//Toast.makeText(getApplicationContext(), "Low battery!", Toast.LENGTH_SHORT).show();
            	break;
            case TGDevice.MSG_RAW_MULTI:
            	//TGRawMulti rawM = (TGRawMulti)msg.obj;
            	//tv.append("Raw1: " + rawM.ch1 + "\nRaw2: " + rawM.ch2);
            	tgRaw = (TGRawMulti)msg.obj;            	
            	im.tv.append("raw: " + 
            	tgRaw.ch1 + ", " + 
            	tgRaw.ch2 + ", " + 
            	tgRaw.ch3 + ", " + 
            	tgRaw.ch4 + ", " + 
            	tgRaw.ch5 + ", " + 
            	tgRaw.ch6 + ", " +
            	tgRaw.ch7 + ", " + 
            	tgRaw.ch8 + ", " + 
            			"\n");
            default:
            	break;
        }		
        		im.tv.setBackgroundColor(Color.argb(255, (int)att, 0, (int)med));        		
        		/*  if (testObserver.hasStrips) {
        			    registry.startPushing();
        			    List<Strip> strips = registry.getStrips();   

        			    for (int y = 0; y < ledsH; y++) {
        			      for (int x = 0; x < ledsW; x++) {
        			    	  int c = ((int)att << 16) | (0 << 8) | (int)med;
        			        if (y < strips.size()) {
        			          strips.get(y).setPixel(c, x);
        			        }
        			      }
        			    }
        			  } 
        			  */      		  
        }  
    };

    public void doStuff() {
    	//should know state and update button to close connection    	
    	if (buttonState == 0){
    	if(tgDevice.getState() != TGDevice.STATE_CONNECTING && tgDevice.getState() != TGDevice.STATE_CONNECTED)
    		tgDevice.connect(true);
//    	writeExtFile("Logging session started: " + DateFormat.getTimeFormat(this).format(t.getTime()) + "\n");
    	//t = Calendar.getInstance();
    	//writeExtFile("Logging session started: " + DateFormat.getTimeFormat(this).format(t.getTime()) + " " + DateFormat.getDateFormat(this).format(t.getTime()) + "\n");
    	im.button.setText("Disconnect");
    	buttonState = 1;
    	}
    	else if(buttonState == 1){
        	if(tgDevice.getState() != TGDevice.STATE_CONNECTING && tgDevice.getState() == TGDevice.STATE_CONNECTED)
        		tgDevice.close();
        	//t = Calendar.getInstance();
        	//writeExtFile("Logging session stopped: " + DateFormat.getTimeFormat(this).format(t.getTime()) + " " + DateFormat.getDateFormat(this).format(t.getTime()) + "\n");
        	//registry.stopPushing();
        	im.button.setText("Connect");
        	buttonState = 0;
        	 wm.clearALL();        
             im.math.setText("");
    	}
    }    
    
    public void onTimmer(){
    	 apim.send_value("let", wm.getLetterValue());	    	 
    	 im.scrn.setText(wm.current_letter);    	 
    	 //String sum = (index%9 + 3) + "x " + (index%9 + 5);
    	 im.math.setText(wm.current_word);	    	 
    	 wm.nextLetter();
    }
	
	public void checkBlink(){
		if(select_mode == 1){
			count_blink++;
		if(blk > 50 && count_blink > 2){
			 wm.pushWord();
			 count_blink =0;		}
		}
	}	
	public void checkAttention(){
		if(select_mode == 0){
		if(att > focus_avg && med < relax_avg - relax_std){
			wm.pushWord();
		}
		}
	}	
	public void preStart(){
		if(med != 0 && att != 0){
		if(values_left > 20){
			values_left--;
			im.scrn.setText("Please focus trying to solve this: "  );
			im.math.setText("7*" + (rand.nextInt(8) + 1 ) );
			focus_avg += att;			
			n_avg++;
			}
		else if(values_left > 0 && values_left < 21){
			values_left--;
			im.scrn.setText("Please now try to relax..");
			im.math.setText("");
			relax_avg += med;
		}
		else if(values_left == 0) { 
			ma.setTimmer();
			relax_avg = relax_avg/20;
			focus_avg = focus_avg/n_avg;
			active = true;
		}		
		}
	}	
	public void DeleteWord(View v) {
        wm.clearALL();        
        im.math.setText("");
      }  
	
	


}
