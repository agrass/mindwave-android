package com.enhanceye.mindwavelogger;



import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.os.Environment;
//import android.os.Environment;
//import com.neurosky.thinkgear.TGDevice;
import android.content.Context;
//import android.util.Log;
import android.widget.Button;
import android.widget.TextView;


import com.heroicrobot.dropbit.registry.*;
import java.util.*;

public class MainActivity extends Activity implements
TextToSpeech.OnInitListener {
	public WordsMgr wm;	
	private TextToSpeech tts;		 
    DeviceRegistry registry;
    public static int activity_mode; 
    TestObserver testObserver;
	public Timer timer;
	public TimerTask task;
    APIMgr am;
    InterfaceMgr im;
    MindWaveMgr mw;
    int ledsW = 48 * 5;
    int ledsH = 8;	
    @Override
    public void onCreate(Bundle savedInstanceState) { 
    	wm = new WordsMgr(this.getAssets());
    	tts = new TextToSpeech(this, this);       	
    	super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        am = new APIMgr();
        im = new InterfaceMgr((TextView)findViewById(R.id.textView1), (TextView)findViewById(R.id.textView2),(TextView)findViewById(R.id.textView3), (Button)findViewById(R.id.button1));
        im.tv.append("Android version: " + android.os.Build.VERSION.RELEASE + "\n" );      
        mw = new MindWaveMgr(im, am, wm, this);
        registry = new DeviceRegistry();
        testObserver = new TestObserver();
        registry.addObserver(testObserver);        
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }



    public void writeFile(String string){
        String FILENAME = "hello_file";
        //String string = "hello world!";
        try {
            FileOutputStream fos = openFileOutput(FILENAME, Context.MODE_APPEND);
            fos.write(string.getBytes());
            fos.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
    }    
    public void writeExtFile(String string){
    	String filename = "mindwavelog.txt";
    	File file = new File(Environment.getExternalStorageDirectory(), filename);
    	FileOutputStream fos;
    	byte[] data = new String(string).getBytes();
    	try {
    	    fos = new FileOutputStream(file, true);
    	    fos.write(data);
    	    fos.flush();
    	    fos.close();
    	} catch (FileNotFoundException e) {
    	    // handle exception
    	} catch (IOException e) {
    	    // handle exception
    	}
    }
	public void onInit(int status) {
		// TODO Auto-generated method stub
		 if (status == TextToSpeech.SUCCESS) {			 
	            int result = tts.setLanguage(Locale.US);	 
	            if (result == TextToSpeech.LANG_MISSING_DATA
	                    || result == TextToSpeech.LANG_NOT_SUPPORTED) {
	                Log.e("TTS", "This Language is not supported");
	            } else {
	                //btnSpeak.setEnabled(true);
	                //speakOut();
	            }	 
	        } else {
	            Log.e("TTS", "Initilization Failed!");
	        }		
	}
	
	//Set timmer b
	public void setTimmer(){			
	//blink_avg= Math.min(blink_avg/3, 55);
	//math.setText("blink avg: " + blink_avg);
	  timer = new Timer();
	  task = new TimerTask() {
	   @Override
	   public void run() {
	    runOnUiThread(new Runnable() {
	     public void run() {
	    	tts.speak(wm.current_letter, TextToSpeech.QUEUE_FLUSH, null);
	    	mw.onTimmer();
	    	
	     }
	    });
	   }
	  };
	  timer.scheduleAtFixedRate(task, 2000, 1000*MindWaveMgr.delay);	  
	}
	//On Press button
	public void onClick(View view){
		mw.doStuff();
	}
}

	

class TestObserver implements Observer {
	  public boolean hasStrips = false;
	  public void update(Observable registry, Object updatedDevice) {
		  java.lang.System.out.println("Registry changed!");
	    if (updatedDevice != null) {
	    	java.lang.System.out.println("Device change: " + updatedDevice);
	    }
	    this.hasStrips = true;
	  }
	}