package com.enhanceye.mindwavelogger;

import java.util.*;
import java.util.Map.Entry;


public class Trie {

    protected final Map<Character, Trie> children;
    LinkedList<Character> priority;
    protected String value;
    protected boolean terminal = false;

    public Trie() {
        this(null);        
    }

    private Trie(String value) {
        this.value = value;
        priority = new LinkedList<Character>();
        children = new HashMap<Character, Trie>();
        
    }

    protected void add(char c) {
        String val;
        if (this.value == null) {
            val = Character.toString(c);
        } else {
            val = this.value + c;
        }
        children.put(c, new Trie(val));
        if(!priority.contains(c)){
        	priority.add(c);
        }
    }

    public void insert(String word) {
        if (word == null) {
            throw new IllegalArgumentException("Cannot add null to a Trie");
        }
        Trie node = this;
        for (char c : word.toCharArray()) {
            if (!node.children.containsKey(c)) {
                node.add(c);
            }
            node = node.children.get(c);
        }
        node.terminal = true;
    }

    public String find(String word) {
        Trie node = this;
        for (char c : word.toCharArray()) {
            if (!node.children.containsKey(c)) {
                return "";
            }
            node = node.children.get(c);
        }
        return node.value;
    }

    public List<String> autoComplete(String prefix) {
        Trie node = this;
        for (char c : prefix.toCharArray()) {
            if (!node.children.containsKey(c)) {
                return Collections.emptyList();
            }
            node = node.children.get(c);
        }
        
        return node.allPrefixes();
    }

    protected List<String> allPrefixes() {
        List<String> results = new ArrayList<String>();
        for(int j = 0; j< priority.size(); j++)
        	results.add(priority.get(j).toString());
        /*Iterator iterator = children.entrySet().iterator();
    	while (iterator.hasNext()) {
    		Map.Entry mapEntry = (Map.Entry) iterator.next();
    		results.add(mapEntry.getKey().toString());    		
    	}
    	*/
    	
        return results;
    }
}
